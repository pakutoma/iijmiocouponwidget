package pakutoma.iijmiocouponwidget.exception;

/**
 * Created by PAKUTOMA on 2016/12/10.
 */
public class NotFoundValidTokenException extends Exception {
    public NotFoundValidTokenException(String str) {
        super(str);
    }
}